import 'package:flutter/material.dart';
import 'package:katlego_duiker/dashboard.dart';

class EditProfile extends StatelessWidget {
  const EditProfile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Edit Profile"),
      ),
      body: Center(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
//Text 1
            const Text("Account Information"),
//Text Field 1
            const TextField(
              obscureText: true,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Edit Name',
              ),
            ),
//Text Field 2
            const TextField(
              obscureText: true,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Change Email',
              ),
            ),
//Text Field 3
            const TextField(
                obscureText: true,
                decoration: InputDecoration(
                  border: OutlineInputBorder(),
                  labelText: 'Change Password',
                )),

//Button 1
            ElevatedButton(
              onPressed: () => {
                Navigator.pop(
                  context,
                  MaterialPageRoute(builder: (context) => const Dashboard()),
                ),
              },
              child: const Text("Save Changes"),
            ),
          ])),
    );
  }
}
