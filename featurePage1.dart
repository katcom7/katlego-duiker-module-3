import 'package:flutter/material.dart';

class FeaturePage1 extends StatelessWidget {
  const FeaturePage1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('Feature Page 1'),
        ),
        body: const Center(
          child: Text('Welcome to Feature Page 1'),
        ));
  }
}
