import 'package:flutter/material.dart';
import 'package:katlego_duiker/editProfile.dart';
import 'package:katlego_duiker/featurePage1.dart';
import 'package:katlego_duiker/featurePage2.dart';
import 'package:katlego_duiker/login.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Dashboard"),
      ),
      body: Center(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
//Text 1
            const Text("Hello User, Welcome to the App"),
            const Text("Play around and have fun with the app"),

//Button 1
            ElevatedButton(
              onPressed: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const FeaturePage1()),
                ),
              },
              child: const Text("Feature Page 1"),
            ),
//Button 2
            ElevatedButton(
              onPressed: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const FeaturePage2()),
                ),
              },
              child: const Text("Feature Page 2"),
            ),
//Button 2
            ElevatedButton(
              onPressed: () => {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const EditProfile()),
                ),
              },
              child: const Text("Edit Profile"),
            ),

    //Floating Action Button
            FloatingActionButton(
              onPressed: () => {
                Navigator.pop(context,
                    MaterialPageRoute(builder: (context) => const Login())),
              },
              child: const Icon(Icons.logout),
            ),

          ])),
    );
  }
}
